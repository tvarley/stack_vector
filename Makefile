MKDIR_P = mkdir -p

.PHONY: clean

BIN=./bin
SRC=./src
LIB=/usr/local/lib/libgtest.a /usr/local/lib/libgtest_main.a -pthread
CXXFLAGS=-O0 -g -std=gnu++17 -ftemplate-depth=1024

BINARIES=$(BIN)/vector_test \
				 $(BIN)/unittest

DIRECTORIES=$(BIN)

all: $(DIRECTORIES) $(BINARIES) 

clean:
	rm -f $(BINARIES)

$(BIN):
	${MKDIR_P} ${BIN}

TESTINC := $(wildcard $(SRC)/test_*.h)
SOLUSRC := $(wildcard $(SRC)/vector*.cpp)
$(SRC)/unittest.o: $(SRC)/unittest.cpp $(TESTINC) $(SOLUSRC)

$(BIN)/unittest: $(SRC)/unittest.o $(SRC)/simple_timer.h $(SRC)/stack_vector.h
		$(CXX) $(CXXFLAGS) $< -o $@ $(LIB)

$(BIN)/%:  $(SRC)/%.o $(SRC)/simple_timer.h $(SRC)/stack_vector.h
	$(CXX) $(CXXFLAGS) $< -o $@
