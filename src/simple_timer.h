#if ! defined SIMPLE_TIMER_INCLUDED_DEFINED
#define SIMPLE_TIMER_INCLUDED_DEFINED

#include <atomic>
#include <chrono>
#include <iostream>

class simple_timer 
{
  std::string message;
  std::chrono::high_resolution_clock::time_point start_point;
  public:
    simple_timer(std::string msg) : message(msg), start_point(std::chrono::high_resolution_clock::now()) {}
    ~simple_timer() {
      stop();
    }
    void stop()
    {
      std::atomic_thread_fence(std::memory_order_relaxed);
      auto end_point = std::chrono::high_resolution_clock::now();
      double counted_time = std::chrono::duration_cast<std::chrono::microseconds>(end_point - start_point).count();
      std::atomic_thread_fence(std::memory_order_relaxed);
      std::cout << message << ": " << counted_time << "μs (" << counted_time / 1000000 << ")" << std::endl;
    }
};

#endif // SIMPLE_TIMER_INCLUDED_DEFINED
