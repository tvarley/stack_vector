#include <iostream>
#include <gtest/gtest.h>

#define UNITTEST_MODE 1 // Exclude local main mostly

int main(int argc, char* argv[] )
{
  ::testing::InitGoogleTest(&argc,argv);
  return RUN_ALL_TESTS();
}
