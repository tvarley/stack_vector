#if ! defined STACK_VECTOR_INCLUDED_DEFINED
#define STACK_VECTOR_INCLUDED_DEFINED

#include <stdexcept>

template <typename T, int reserve> class stack_vector 
{
  T data[reserve];
  int used;
  public:

  stack_vector() : used(0) {}

  void push_back(const T& val) {
    if(used == reserve ) {
      throw new std::runtime_error("stack_vector full");
    }
    data[used++] = val;
  }
};

template <typename T> using small_stack_vector = stack_vector<T, 100>;
template <typename T> using medium_stack_vector = stack_vector<T, 1000>;
template <typename T> using large_stack_vector = stack_vector<T, 10000>;

#endif // STACK_VECTOR_INCLUDED_DEFINED
