#include <iostream>
#include <sstream>
#include <vector>

#include "simple_timer.h"
#include "stack_vector.h"

void stack_vector_test(int vector_size, int iter_count)
{
  std::ostringstream formatted;
  formatted << "stack_vector: " << iter_count << " * " << vector_size;
  {
    simple_timer x(formatted.str());

    for(int i{iter_count} ; i-- ; ) {
      large_stack_vector<int> v;
      for(int j{iter_count} ; j-- ; ) {
        v.push_back(j);
      }
    }
  }
}

void std_vector_test(int vector_size, int iter_count)
{
  std::ostringstream formatted;
  formatted << "std::vector: " << iter_count << " * " << vector_size;
  {
    simple_timer x(formatted.str());

    for(int i{iter_count} ; i-- ; ) {
      std::vector<int> v;
      v.reserve(vector_size);
      for(int j{iter_count} ; j-- ; ) {
        v.push_back(j);
      }
    }
  }
}

int main(int argc, char* argv[]) 
{
  std_vector_test(100,10000);
  std_vector_test(1000,10000);
  std_vector_test(10000,10000);

  stack_vector_test(100,10000);
  stack_vector_test(1000,10000);
  stack_vector_test(10000,10000);
}
